﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace nested_tab_challenge.Main.A
{
    public class AViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        bool m_isFirstVisible;
        bool m_isSecondVisible;

        public bool IsFirstVisible
        {
            get
            {
                return m_isFirstVisible;
            }
            set
            {
                m_isFirstVisible = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsFirstVisible)));
            }
        }

        public bool IsSecondVisible
        {
            get
            {
                return m_isSecondVisible;
            }
            set
            {
                m_isSecondVisible = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSecondVisible)));
            }
        }

        public ICommand SelectTabCommand
        {
            get;
            private set;
        }

        public AViewModel()
        {
            SelectTab("first");
            SelectTabCommand = new RelayCommand(SelectTab);
        }

        void SelectTab(object param)
        {
            string name = param as string;

            if (string.IsNullOrEmpty(name))
            {
                return;
            }

            IsFirstVisible = name == "first";
            IsSecondVisible = name == "second";
        }
    }
}
