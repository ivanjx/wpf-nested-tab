﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace nested_tab_challenge.Main
{
    public class RootViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        bool m_isAVisible;
        bool m_isBVisible;
        bool m_isCVisible;

        public bool IsAVisible
        {
            get
            {
                return m_isAVisible;
            }
            set
            {
                m_isAVisible = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsAVisible)));
            }
        }

        public bool IsBVisible
        {
            get
            {
                return m_isBVisible;
            }
            set
            {
                m_isBVisible = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsBVisible)));
            }
        }

        public bool IsCVisible
        {
            get
            {
                return m_isCVisible;
            }
            set
            {
                m_isCVisible = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsCVisible)));
            }
        }

        public ICommand SelectTabCommand
        {
            get;
            private set;
        }

        public RootViewModel()
        {
            SelectTab("A");
            SelectTabCommand = new RelayCommand(SelectTab);
        }

        void SelectTab(object param)
        {
            string tab = param as string;

            if (string.IsNullOrEmpty(tab))
            {
                return;
            }

            IsAVisible = tab == "A";
            IsBVisible = tab == "B";
            IsCVisible = tab == "C";
        }
    }
}
