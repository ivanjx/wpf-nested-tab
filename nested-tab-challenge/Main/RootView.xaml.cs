﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace nested_tab_challenge.Main
{
    /// <summary>
    /// Interaction logic for RootView.xaml
    /// </summary>
    public partial class RootView : UserControl
    {
        RootViewModel m_vm;

        public RootView()
        {
            InitializeComponent();

            m_vm = new RootViewModel();
            root.DataContext = m_vm;
        }
    }
}
